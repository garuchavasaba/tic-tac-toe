package com.example.game

import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    public var playerOne = true

    private fun init() {
        button00.setOnClickListener {
            checkIfButtonIsEmpty(button00)
        }
        button01.setOnClickListener {
            checkIfButtonIsEmpty(button01)
        }
        button02.setOnClickListener {
            checkIfButtonIsEmpty(button02)
        }
        button10.setOnClickListener {
            checkIfButtonIsEmpty(button10)
        }
        button11.setOnClickListener {
            checkIfButtonIsEmpty(button11)
        }
        button12.setOnClickListener {
            checkIfButtonIsEmpty(button12)
        }
        button20.setOnClickListener {
            checkIfButtonIsEmpty(button20)
        }
        button21.setOnClickListener {
            checkIfButtonIsEmpty(button21)
        }
        button22.setOnClickListener {
            checkIfButtonIsEmpty(button22)
        }

    }

    private fun checkIfButtonIsEmpty(button: Button) {
        if (button.text.isEmpty()) {
            if (playerOne) {
                button.text = "X"
                playerOne = false
            } else {
                button.text = "O"
                playerOne = true
            }
        }
        if (button00.text.isEmpty() && button00.text == button01.text && button01.text == button02.text) {
            Toast.makeText(this, "Winner is ${button00.text}", Toast.LENGTH_SHORT).show()
            }       else if (button10.text.isEmpty() && button10.text == button11.text && button12.text == button10.text) {
                Toast.makeText(this, "Winner is ${button10.text}", Toast.LENGTH_LONG)
            } else if (button20.text.isEmpty() && button20.text == button21.text && button22.text == button20.text) {
                Toast.makeText(this, "Winner is ${button20.text}", Toast.LENGTH_LONG)
            } else if (button00.text.isEmpty() && button10.text == button00.text && button20.text == button00.text) {
                Toast.makeText(this, "Winner is ${button00.text}", Toast.LENGTH_LONG)
            } else if (button01.text.isEmpty() && button11.text == button01.text && button11.text == button21.text) {
                Toast.makeText(this, "Winner is ${button01.text}", Toast.LENGTH_LONG)
            } else if (button02.text.isEmpty() && button12.text == button01.text && button02.text == button22.text) {
                Toast.makeText(this, "Winner is ${button02.text}", Toast.LENGTH_LONG)
            } else if (button00.text.isEmpty() && button11.text == button00.text && button00.text == button22.text) {
                Toast.makeText(this, "Winner is ${button00.text}", Toast.LENGTH_LONG)
            } else if (button02.text.isEmpty() && button11.text == button02.text && button02.text == button00.text) {
                Toast.makeText(this, "Winner is ${button02.text}", Toast.LENGTH_LONG)
            }
        }

    }
